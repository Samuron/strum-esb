# frozen_string_literal: true

RSpec.describe Strum::Esb do
  it "has a version number" do
    expect(Strum::Esb::VERSION).not_to be nil
  end
end
