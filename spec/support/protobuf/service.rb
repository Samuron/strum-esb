# frozen_string_literal: true

GrpcTestDesc = Struct.new(:name, :input, :marshal_method, :unmarshal_method, keyword_init: true)

class ProtobufService
  def self.rpc_descs
    {
      ActionUpdateNameRequest: GrpcTestDesc.new(
        name: :ActionUpdateNameRequest,
        input: ProtobufMessage,
        marshal_method: :encode,
        unmarshal_method: :decode
      )
    }
  end
end
