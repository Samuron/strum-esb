# frozen_string_literal: true

module Strum
  module Esb
    # Strum Message
    class Message
      class << self
        def publish(exchange:, headers:, payload:, **args)
          rabbit_exchange ||= Strum::Esb.config.rabbit_channel_pool.with do |rabbit_channel|
            rabbit_channel.headers(exchange, durable: true)
          end

          properties = { headers: headers, content_type: args.fetch(:content_type, "application/json") }

          Strum::Esb.config.before_publish_hooks.each { |hook| hook.call(payload, properties) }
          args.merge!(properties)

          payload, valid_payload = Strum::Esb.config.serializer.serialize(payload, args: args)
          return unless valid_payload

          rabbit_exchange.publish(payload, **properties)
        end

        def extend_headers(properties)
          properties[:headers] = {} unless properties[:headers].is_a?(Hash)
          properties[:headers]["pipeline"] ||= Thread.current[:pipeline] if Thread.current[:pipeline]
          properties[:headers]["pipeline-id"] ||= Thread.current[:pipeline_id] if Thread.current[:pipeline_id]
        end
      end
    end
  end
end
