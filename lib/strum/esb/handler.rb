# frozen_string_literal: true

require "json"
require "strum/esb/functions"

module Strum
  module Esb
    # Sneakers helper for Strum
    module Handler
      def self.included(base)
        base.class_eval do
          include Sneakers::Worker
        end
        base.extend ClassMethods
        base.use_application_config
      end

      module ClassMethods
        def bind_to(queue, message_type, message_binding, handler = nil)
          bindings ||= (queue_opts && queue_opts[:bindings]) || {}

          if block_given?
            yield(bindings)
          else
            bindings[message_type] ||= []
            bindings[message_type] << message_binding
          end

          from_queue queue, bindings: bindings
          handlers[handler_key(message_type, message_binding)] = handler.to_s if handler
        end

        def handler_key(message_type, message_binding)
          _, *msg = Strum::Esb::Functions.public_send("#{message_type}_explain", message_binding)
          params = msg.map { |param| param&.to_s&.gsub(/[^a-zA-Z0-9]/, "_")&.downcase }
          ([message_type] + params).join("-")
        end

        def handlers
          @handlers ||= {}
        end

        def enable_json(switcher)
          @json_mode = switcher
        end

        def json_enabled?
          @json_mode
        end

        def enable_protobuf(switcher)
          @protobuf_mode = switcher
        end

        def protobuf_enabled?
          @protobuf_mode
        end

        def content_type_enabled?(content_type)
          case content_type
          when "application/json"
            json_enabled?
          when "application/x-protobuf"
            protobuf_enabled?
          else
            false
          end
        end

        def use_protobuf_service(service)
          raise ArgumentError, "#{service} must be a grpc service" unless service.respond_to?(:rpc_descs)

          @protobuf_service = service
          enable_protobuf(true)
        end

        def protobuf_service
          @protobuf_service
        end

        def use_application_config
          enable_json(Strum::Esb.config.enable_json)
          enable_protobuf(Strum::Esb.config.enable_protobuf)
        end
      end

      def header(metadata, key)
        (metadata[:headers] && metadata[:headers][key])&.to_s&.gsub(/[^a-zA-Z0-9]/, "_")&.downcase
      end

      def work_with_params(deserialized_msg, delivery_info, metadata)
        Strum::Esb.config.before_handler_hooks.each { |hook| hook.call(deserialized_msg, delivery_info, metadata) }

        action = header(metadata, "action")
        resource = header(metadata, "resource")
        event = header(metadata, "event")
        state = header(metadata, "state")
        info = header(metadata, "info")
        notice = header(metadata, "notice")
        pipeline = Thread.current[:pipeline] = header(metadata, "pipeline")
        pipeline_id = Thread.current[:pipeline_id] = header(metadata, "pipeline-id")

        after_headers_hook(binding)

        methods_names = if action
                          action_handler_methods(action, resource)
                        elsif event
                          event_handler_methods(resource, event, state)
                        elsif info
                          info_handler_methods(info)
                        elsif notice
                          notice_handler_methods(resource, notice)
                        end

        method_name = ([*methods_names] << "handler").find { |n| respond_to?(n, include_all: true) }

        unless method_name
          logger.error "Handler not found. Message rejected #{metadata[:headers]} with payload #{deserialized_msg}"
          return reject!
        end

        content_type = metadata[:content_type] || "application/json"

        unless self.class.content_type_enabled?(content_type)
          logger.error "Content type #{content_type} is disabled by handler. Message rejected #{metadata[:headers]} with payload #{deserialized_msg}"
          return reject!
        end

        args = { queue_method_name: method_name, grpc_service: self.class.protobuf_service, content_type: content_type }

        payload, valid_payload = Strum::Esb.config.serializer.deserialize(deserialized_msg, args: args)

        unless valid_payload
          logger.error "Payload was unable to be parsed with #{content_type} content type. Message rejected #{metadata[:headers]} with payload #{payload}"
          return reject!
        end

        error = nil
        method_params = method(method_name)
                        .parameters
                        .map { |param| _, param_name = param; param_name }
                        .then { |m| m & %I[action resource event state info pipeline pipeline_id] }
        handler_params = method_params.each_with_object({}) { |i, res| res[i] = eval(i.to_s); }
        logger.info("Handler #{method_name} found. Payload: #{payload}")
        handler_params.count.positive? ? send(method_name, payload, handler_params) : send(method_name, payload)
        logger.info("Handler #{method_name} executed")
        ack!
      rescue StandardError => e
        error = e
        logger.error e
        reject!
      ensure
        Strum::Esb.config.after_handler_hooks.each { |hook| hook.call(deserialized_msg, delivery_info, metadata, payload, error) }
      end

      def after_headers_hook(bind); end

      private

        def action_handler_methods(action, resource)
          if (custom_handler = self.class.handlers[["action", action, resource].join("-")])
            %W[#{custom_handler} action_#{action}_#{resource} action_handler]
          else
            %W[action_#{action}_#{resource} action_handler]
          end
        end

        def event_handler_methods(resource, event, state)
          result = []
          custom_handler = self.class.handlers[["event", resource, event, state].join("-")]
          result << custom_handler if custom_handler
          result << "event_#{resource}_#{event}_#{state}"
          result << "event_#{resource}_#{event}" if state.eql?("success")
          result << "event_handler"
          result
        end

        def info_handler_methods(info)
          if (custom_handler = self.class.handlers[["info", info].join("-")])
            %W[#{custom_handler} info_#{info} info_handler]
          else
            %W[info_#{info} info_handler]
          end
        end

        def notice_handler_methods(resource, notice)
          custom_handler = self.class.handlers[["notice", resource, notice].join("-")]
          result = []
          result << custom_handler if custom_handler
          result << "notice_#{resource}_#{notice}" if resource
          result << "notice_#{notice}"
          result << "notice_handler"
          result
        end
    end
  end
end
